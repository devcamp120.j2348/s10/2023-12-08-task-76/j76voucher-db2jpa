package com.devcamp.pizza365.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Book {
    @EmbeddedId
    private BookId bookId;
    
    private long price;

    @ManyToOne
    CVoucher voucher;
    public Book() {
    }

    public Book(BookId bookId, long price) {
        this.bookId = bookId;
        this.price = price;
    }

    public Book(String language, String title, long price) {
        this.bookId = new BookId(title, language);
        this.price = price;
    }

    public BookId getBookId() {
        return bookId;
    }

    public void setBookId(BookId bookId) {
        this.bookId = bookId;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

}
